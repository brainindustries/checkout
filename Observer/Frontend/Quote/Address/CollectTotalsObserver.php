<?php

namespace Shirtplatform\Checkout\Observer\Frontend\Quote\Address;

class CollectTotalsObserver extends \Magento\Quote\Observer\Frontend\Quote\Address\CollectTotalsObserver {

    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * Initialize dependencies.
     *
     * @param \Magento\Customer\Helper\Address $customerAddressHelper
     * @param \Magento\Customer\Model\Vat $customerVat
     * @param \Magento\Quote\Observer\Frontend\Quote\Address\VatValidator $vatValidator
     * @param \Magento\Customer\Api\Data\CustomerInterfaceFactory $customerDataFactory
     * @param \Magento\Customer\Api\GroupManagementInterface $groupManagement
     * @param \Magento\Customer\Api\AddressRepositoryInterface $addressRepository
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(\Magento\Customer\Helper\Address $customerAddressHelper,
                                \Magento\Customer\Model\Vat $customerVat,
                                \Magento\Quote\Observer\Frontend\Quote\Address\VatValidator $vatValidator,
                                \Magento\Customer\Api\Data\CustomerInterfaceFactory $customerDataFactory,
                                \Magento\Customer\Api\GroupManagementInterface $groupManagement,
                                \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
                                \Magento\Customer\Model\Session $customerSession
    ) {
        parent::__construct($customerAddressHelper, $customerVat, $vatValidator, $customerDataFactory, $groupManagement, $addressRepository, $customerSession);
        $this->addressRepository = $addressRepository;
        $this->customerSession = $customerSession;
    }

    /**
     * Handle customer VAT number if needed on collect_totals_before event of quote address
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        /** @var \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment */
        $shippingAssignment = $observer->getShippingAssignment();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getQuote();
        /** @var \Magento\Quote\Model\Quote\Address $address */
        $address = $shippingAssignment->getShipping()->getAddress();

        $customer = $quote->getCustomer();
        $storeId = $customer->getStoreId();

        if ($customer->getDisableAutoGroupChange() || false == $this->vatValidator->isEnabled($address, $storeId)
        ) {
            return;
        }
        $customerCountryCode = $address->getCountryId();
        $customerVatNumber = $address->getVatId();

        /** try to get data from customer if quote address needed data is empty */
        if (empty($customerCountryCode) && empty($customerVatNumber) && $customer->getDefaultShipping()) {
            $customerAddress = $this->addressRepository->getById($customer->getDefaultShipping());

            $customerCountryCode = $customerAddress->getCountryId();
            $customerVatNumber = $customerAddress->getVatId();
        }        

        $groupId = null;
        if (empty($customerVatNumber) || false == $this->customerVat->isCountryInEU($customerCountryCode)) {
            $groupId = $customer->getId() ? $this->groupManagement->getDefaultGroup(
                            $storeId
                    )->getId() : $this->groupManagement->getNotLoggedInGroup()->getId();            
        }
        else {
            // Magento always has to emulate group even if customer uses default billing/shipping address
            $groupId = $this->customerVat->getCustomerGroupIdBasedOnVatNumber(
                    $customerCountryCode, $this->vatValidator->validate($address, $storeId), $storeId
            );            
        }        

        //I removed a condition if $groupId is true, because notLoggedIn group has ID = 0
        //and it didn't calculate the tax in this case
        $address->setPrevQuoteCustomerGroupId($quote->getCustomerGroupId());
        $quote->setCustomerGroupId($groupId);
        $this->customerSession->setCustomerGroupId($groupId);
        $customer->setGroupId($groupId);
        $email = $quote->getCustomerEmail();
        $quote->setCustomer($customer);
        
        //when initializing order in admin, email information might be lost
        if (!$quote->getCustomerEmail()) {
            $quote->setCustomerEmail($email);
        }
    }

}
