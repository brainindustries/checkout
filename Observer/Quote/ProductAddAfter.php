<?php

namespace Shirtplatform\Checkout\Observer\Quote;

use Magento\Framework\Event\ObserverInterface;

class ProductAddAfter implements ObserverInterface
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $_productRepository;
    
    /**     
     * @access public
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @return void
     */
    public function __construct(\Magento\Catalog\Api\ProductRepositoryInterface $productRepository)
    {
        $this->_productRepository = $productRepository;
    }
    
    /**
     * Clean product repository cache after product is added to cart. If not done so,
     * it creates problems when adding team products or the same combination (size + color) in admin
     * in combination with Mollie extension as there are missing records in quote_item_option table     
     *
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->_productRepository->cleanCache();
    }
}