<?php

namespace Shirtplatform\Checkout\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\PaymentMethodManagement;
use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Payment\Model\Checks\SpecificationFactory;
use Magento\Quote\Model\Quote\PaymentFactory;

class QuoteCollectTotalsBefore implements ObserverInterface {

    private $_paymentMethodManagement;
    private $_specificationFactory;
    private $_paymentFactory;

    /**
     * 
     * @access public
     * @param \Magento\Quote\Model\PaymentMethodManagement $paymentMethodManagement
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     */
    public function __construct(
        PaymentMethodManagement $paymentMethodManagement,
        SpecificationFactory $specificationFactory,
        PaymentFactory $paymentFactory
    ){
        $this->_paymentMethodManagement = $paymentMethodManagement;
        $this->_specificationFactory = $specificationFactory;
        $this->_paymentFactory = $paymentFactory;
    }

    /**
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $quote = $observer->getQuote();
        
        if (!$quote->getPayment()->getMethod()){
            return;
        }

        $method = $quote->getPayment()->getMethodInstance();

        $specification = $this->_specificationFactory->create([
            AbstractMethod::CHECK_USE_FOR_COUNTRY,
            AbstractMethod::CHECK_USE_FOR_CURRENCY,
            AbstractMethod::CHECK_ORDER_TOTAL_MIN_MAX,
        ]);

        if (!$specification->isApplicable($method, $quote)){
            $quote->getPayment()->setMethod(null);
            $payment = $this->_paymentFactory->create();
            $payment->setMethod('banktransfer');
            $quote->setPayment($payment);
        }
    }

}