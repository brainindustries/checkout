<?php

namespace Shirtplatform\Checkout\Observer;

use Magento\Framework\Event\ObserverInterface;
use Shirtplatform\Core\Helper\Data as CoreHelper;
use shirtplatform\entity\order\Order;

class QuoteRemoveItem implements ObserverInterface
{

    /**
     * @var CoreHelper
     */
    private $_coreHelper;
    
    /**
     * @param CoreHelper $coreHelper
     */
    public function __construct(CoreHelper $coreHelper)
    {
        $this->_coreHelper = $coreHelper;
    }

    /**
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quoteItem = $observer->getQuoteItem();

        $this->_coreHelper->shirtplatformAuth($quoteItem->getStoreId());
        $platformOrder = Order::find($quoteItem->getShirtplatformOrigOrderId());

        $platformOrderProducts = $platformOrder->getOrderedProducts();

        if (is_array($platformOrderProducts) && count($platformOrderProducts) === 0) {
            $platformOrder->__delete();
        }
    }

}