<?php

namespace Shirtplatform\Checkout\Observer;

use Magento\Framework\Event\ObserverInterface;

class QuoteSubmitBefore implements ObserverInterface {

    /**
     * Copy extension attributes from quote to order and quote address to 
     * order address
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $quote = $observer->getQuote();
        $order = $observer->getOrder();

        $order->setDeliveryBranchId($quote->getDeliveryBranchId());
        $order->setDeliveryBranchName($quote->getDeliveryBranchName());

        $orderShippingAddress = $order->getShippingAddress();
        if ($orderShippingAddress) {
            $quoteShippingAddress = $quote->getShippingAddress();
            $orderShippingAddress->setStreetNumber($quoteShippingAddress->getStreetNumber());
            $orderShippingAddress->setCareOf($quoteShippingAddress->getCareOf());
            $orderShippingAddress->setBusinessNumber($quoteShippingAddress->getBusinessNumber());

            //sometimes the attributes can be set in extension_attributes
            $extensionAttributes = $quoteShippingAddress->getExtensionAttributes();
            if ($extensionAttributes) {
                if (empty($orderShippingAddress->getStreetNumber())) {
                    if (method_exists($extensionAttributes, 'getStreetNumber')) {
                        $orderShippingAddress->setStreetNumber($extensionAttributes->getStreetNumber());
                    }
                }

                if (empty($orderShippingAddress->getCareOf())) {
                    if (method_exists($extensionAttributes, 'getCareOf')) {
                        $orderShippingAddress->setCareOf($extensionAttributes->getCareOf());
                    }
                }
            }
        }

        $orderBillingAddress = $order->getBillingAddress();
        $quoteBillingAddress = $quote->getBillingAddress();

        $orderBillingAddress->setStreetNumber($quoteBillingAddress->getStreetNumber());
        $orderBillingAddress->setCareOf($quoteBillingAddress->getCareOf());
        $orderBillingAddress->setBusinessNumber($quoteBillingAddress->getBusinessNumber());

        //sometimes the attributes can be set in extension_attributes
        $extensionAttributes = $quoteBillingAddress->getExtensionAttributes();
        if ($extensionAttributes) {
            if (empty($orderBillingAddress->getStreetNumber())) {
                if (method_exists($extensionAttributes, 'getStreetNumber')) {
                    $orderBillingAddress->setStreetNumber($extensionAttributes->getStreetNumber());
                }
            }

            if (empty($orderBillingAddress->getCareOf())) {
                if (method_exists($extensionAttributes, 'getCareOf')) {
                    $orderBillingAddress->setCareOf($extensionAttributes->getCareOf());
                }
            }
        }
    }

}
