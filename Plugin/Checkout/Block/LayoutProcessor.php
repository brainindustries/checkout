<?php

namespace Shirtplatform\Checkout\Plugin\Checkout\Block;

class LayoutProcessor {

    /**
     * @var \Magento\Checkout\Helper\Data
     */
    private $_checkoutDataHelper;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * 
     * @param \Magento\Checkout\Helper\Data $checkoutDataHelper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Checkout\Helper\Data $checkoutDataHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_checkoutDataHelper = $checkoutDataHelper;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Set validation rule required-entry to false for business_number in billing and
     * shipping address form fields if it's not set
     * 
     * @access public
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess($subject,
                                 $jsLayout) {
        //billing address
        if ($this->_checkoutDataHelper->isDisplayBillingOnPaymentMethodAvailable()) {
            foreach ($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'] as $key => $child) {
                //not a payment method
                if (!isset($child['children']['form-fields'])) {
                    continue;
                }

                if (isset($child['children']['form-fields']['children']['business_number'])) {
                    $businessNumber = $child['children']['form-fields']['children']['business_number'];

                    if (!isset($businessNumber['validation']['required-entry'])) {
                        $businessNumber['validation']['required-entry'] = false;
                        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'][$key]['children']['form-fields']['children']['business_number'] = $businessNumber;                        
                    }
                }
            }
        }
        else {
            if (isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['business_number'])) {
                $businessNumber = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['business_number'];

                if (!isset($businessNumber['validation']['required-entry'])) {
                    $businessNumber['validation']['required-entry'] = false;
                    $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['business_number'] = $businessNumber;
                }
            }
        }
        
        //shipping address
        $usePackstation = $this->_scopeConfig->getValue('checkout/options/use_packstation', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['business_number'])) {
            $businessNumber = $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['business_number'];
            
            if (!isset($businessNumber['validation']['required_entry'])) {
                $businessNumber['validation']['required-entry'] = false;
                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['business_number'] = $businessNumber;
            }
        }                
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['street']) && $usePackstation) {
            $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['street']['config']['template'] = 'Shirtplatform_Checkout/ui-element/group/group';
        }
        
        return $jsLayout;
    }

}
