<?php

namespace Shirtplatform\Checkout\Plugin\Checkout\Model;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Directory\Model\PriceCurrency;
use Magento\Quote\Api\Data\CartExtension;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Tax\Helper\Data as TaxData;
use Magento\Tax\Model\TaxCalculation;
use Shirtplatform\Shipping\Helper\Data;
use Magento\Shipping\Model\Config;
use Shirtplatform\Shipping\Helper\Data as ShippingHelper;

class DefaultConfigProvider {

    const ZASIELKOVNA_CODE = 23;

    /** @var CheckoutSession */
    private $_checkoutSession;

    /** @var ScopeConfigInterface */
    private $_scopeConfig;

    /** @var PriceCurrency */
    private $_priceCurrency;

    /** @var SerializerInterface */
    private $_serializer;

    /** @var Data */
    private $_shirtplatformData;

    /** @var TaxCalculation */
    private $_taxCalculation;

    /** @var TaxData */
    private $_taxData;

    /** @var Config */
    private $_shippingModelConfig;

    /** @var ShippingHelper */
    private $_shippingHelper;

    public function __construct(
        CheckoutSession $checkoutSession,
        ScopeConfigInterface $scopeConfig,
        Data $shirtplatformData,
        PriceCurrency $priceCurrency,
        SerializerInterface $serializer,
        TaxCalculation $taxCalculation,
        TaxData $taxData,
        Config $shippingModelConfig,
        ShippingHelper $shippingHelper
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_scopeConfig = $scopeConfig;
        $this->_priceCurrency = $priceCurrency;
        $this->_serializer = $serializer;
        $this->_shirtplatformData = $shirtplatformData;
        $this->_taxCalculation = $taxCalculation;
        $this->_taxData = $taxData;
        $this->_shippingModelConfig = $shippingModelConfig;
        $this->_shippingHelper = $shippingHelper;
    }

    /**
     * @param \Magento\Checkout\Model\DefaultConfigProvider $subject
     * @param array $result
     * @return array
     */
    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject,
                                   array $result) {
        $quote = $this->_checkoutSession->getQuote();
        if ($quote == null) {
            return $result;
        }

        $extensionAttributes = $result['quoteData']['extension_attributes']['shipping_assignments'][0] ?? new CartExtension();
        $extensionAttributes->setData('delivery_branch_id', $quote->getDeliveryBranchId());
        $extensionAttributes->setData('delivery_branch_name', $quote->getDeliveryBranchName());

        if ($extensionAttributes instanceof CartExtension) {
            $extensionAttributesData = $extensionAttributes->__toArray();
        }
        else {
            $extensionAttributesData = $extensionAttributes->getData();
        }

        $data = [];
        foreach ($extensionAttributesData as $key => $value) {
            if (is_string($value)) {
                $data[$key] = $value;
            }
        }
        $result['quoteData']['extension_attributes'] = $data;
        $result['usePackstation'] = $this->_scopeConfig->getValue(
                'checkout/options/use_packstation', ScopeInterface::SCOPE_STORE, $quote->getStoreId()
        );
        $result['zasielkovna_method_name'] = 'shirtplatformShipping_' . self::ZASIELKOVNA_CODE;
        $result['zasielkovna_key'] = $this->_scopeConfig->getValue(
                'carriers/shirtplatformShipping/zasielkovna_key', ScopeInterface::SCOPE_STORE, $quote->getStoreId()
        );
        $result['packetery_method_name'] = 'packetery_pickupPointDelivery';
        $result['shippingMethod_logo'] = json_encode($this->_shirtplatformData->getProviderLogoMapping($quote->getStoreId()));
        $result['shippingMethod_description'] = $this->_getShippingMethodDescription($quote);
        $result['isAdditionalShippingLogicEnabled'] = (int)$this->_shirtplatformData->isAdditionalLogicEnabled($quote->getStoreId());
        $result['additionalShippingData'] = $this->_getAdditionalShippingData($quote->getStore());
        
        $result['isFreeshippingEnabled'] = (int)$this->_scopeConfig->getValue(
                'carriers/freeshipping/active', ScopeInterface::SCOPE_STORE, $quote->getStoreId());
        $result['freeshippingSubtotalMin'] = $this->_scopeConfig->getValue(
                'carriers/freeshipping/free_shipping_subtotal', ScopeInterface::SCOPE_STORE, $quote->getStoreId());      
        
        $shippingMapping = $this->_scopeConfig->getValue('shirtplatform_productiontool/shipping_mapping/mapping', ScopeInterface::SCOPE_STORE, $quote->getStoreId());
        if (empty($shippingMapping)){
            $shippingMapping = [];
        } else {
            $shippingMapping = $this->_serializer->unserialize($shippingMapping);
        }
        
        $freeshippingMethod = null;
        foreach ($shippingMapping as $method){
            if ($method['src_method'] == 'freeshipping_freeshipping'){
                $freeshippingMethod = $method['dst_method'];
                break;
            }
        }
        $result['freeshippingCarrier'] = (int)$freeshippingMethod;

        $result['isBusinessNumberRequiredForVatId'] = (int)$this->_scopeConfig->getValue(
                'checkout/options/is_business_number_required_for_vat_id', ScopeInterface::SCOPE_STORE, $quote->getStoreId()
        );

        $result['locale'] = $this->_scopeConfig->getValue('general/locale/code', ScopeInterface::SCOPE_STORE, $quote->getStoreId());
        
        return $result;
    }


    private function _getAdditionalShippingData($store){
        $titleDhl = $this->_scopeConfig->getValue(
            'carriers/shirtplatformAdditionalLogic/dhl_title', ScopeInterface::SCOPE_STORE, $store->getStoreId());

        $nameDhl = $this->_scopeConfig->getValue(
            'carriers/shirtplatformAdditionalLogic/dhl_name', ScopeInterface::SCOPE_STORE, $store->getStoreId());
        
        $titleDepost = $this->_scopeConfig->getValue(
            'carriers/shirtplatformAdditionalLogic/depost_title', ScopeInterface::SCOPE_STORE, $store->getStoreId());

        $nameDepost = $this->_scopeConfig->getValue(
            'carriers/shirtplatformAdditionalLogic/depost_name', ScopeInterface::SCOPE_STORE, $store->getStoreId());
        
        $price = $this->_scopeConfig->getValue(
            'carriers/shirtplatformAdditionalLogic/price', ScopeInterface::SCOPE_STORE, $store->getStoreId());


        if ($this->_taxData->shippingPriceIncludesTax()){
            $taxRate = $this->_taxCalculation->getCalculatedRate($this->_taxData->getShippingTaxClass($store));

            $priceInclTax = $price;
            $priceExclTax = $this->_priceCurrency->convertAndRound($price / (1+($taxRate/100)));
        } else {
            $priceInclTax = $this->_taxData->getShippingPrice($price, true);
            $priceExclTax = $price;
        }
        

        return [
            'title_'. $this->_shirtplatformData::PROVIDER_DHL_STANDARD => $titleDhl,
            'name_'. $this->_shirtplatformData::PROVIDER_DHL_STANDARD => $nameDhl,
            'title_'. $this->_shirtplatformData::PROVIDER_DEUTCHEPOST => $titleDepost,
            'name_'. $this->_shirtplatformData::PROVIDER_DEUTCHEPOST => $nameDepost,
            'price_excl_tax' => $priceExclTax,
            'price_incl_tax' => $priceInclTax
        ];
    }

    private function _getShippingMethodDescription($quote){
        $result = $this->_shirtplatformData->getProviderLocalizationsMapping($quote->getStoreId());        
        $localeCode = $this->_scopeConfig->getValue('general/locale/code', ScopeInterface::SCOPE_STORE, $quote->getStoreId());
        $countryName = '';

        $availableLocales = \Yasumi\Yasumi::getAvailableLocales();
        if (in_array($localeCode, $availableLocales)){
            $countryCode = strtoupper(substr($localeCode, strpos($localeCode, '_') + 1));
            $providers = \Yasumi\Yasumi::getProviders();
            if (isset($providers[$countryCode])){
                $countryName = $providers[$countryCode];
            }
        }

        $result['freeshipping']['description'] = $this->_scopeConfig->getValue('carriers/freeshipping/description', ScopeInterface::SCOPE_STORE, $quote->getStoreId());

        if ($countryName){
            $shippings = $this->_shippingModelConfig->getActiveCarriers();
            $shirtplatformShippings = [
                'shirtplatform_basic', 
                'shirtplatform_standard', 
                'shirtplatform_premium', 
                'shirtplatform_express'                
            ];
            foreach($shippings as $shippingCode => $shippingModel){
                if (in_array($shippingCode, $shirtplatformShippings)){
                    // check if delivery calculation is enabled
                    $deliveryCalculation = $shippingModel->getConfigData('delivery_calculation');
                    if (!$deliveryCalculation){
                        continue;
                    }
                    
                    $largeOrderPrefix = $this->_shippingHelper->isLargeOrder($quote->getSubtotal(), $quote->getItemsQty()) ? 'large_order_' : '';                                                                                    
                    $result[$shippingCode] = [
                        'description' => __('Delivery expected %1', $this->_getExpectedDelivery($countryName, $shippingModel, $largeOrderPrefix))
                    ];
                }
                elseif ($shippingCode == 'shirtplatformShipping' && isset($result[self::ZASIELKOVNA_CODE])) {
                    // check if delivery calculation is enabled
                    $deliveryCalculation = $shippingModel->getConfigData('zasielkovna_delivery_calculation');
                    if (!$deliveryCalculation){
                        continue;
                    }
                
                    $result[self::ZASIELKOVNA_CODE]['description'] = !empty($result[self::ZASIELKOVNA_CODE]['description']) ? $result[self::ZASIELKOVNA_CODE]['description'] . '<br>' : '';
                    $result[self::ZASIELKOVNA_CODE]['description'] .= __('Delivery expected %1', $this->_getExpectedDelivery($countryName, $shippingModel, 'zasielkovna_'));                    
                }
                elseif ($shippingCode == 'freeshipping'){
                    $deliveryCalculation = $shippingModel->getConfigData('delivery_calculation');
                    if (!$deliveryCalculation){
                        continue;
                    }   

                    $result['freeshipping']['description'] .= '<br>' . __('Delivery expected %1', $this->_getExpectedDelivery($countryName, $shippingModel));
                }
            }            
        }
        
        return json_encode($result);
    }
    
    /**
     * Get expected delivery dates (from - to) as string
     *
     * @access private
     * @param string $countryName
     * @param  mixed $shippingModel
     * @param string $configPrefix (e.g. 'large_order_' or 'zasielkovna_')
     * @return string
     */
    private function _getExpectedDelivery($countryName, $shippingModel, $configPrefix = '')
    {        
        // get min and max days from config
        $daysMin = (int)$shippingModel->getConfigData($configPrefix . 'days_min');
        $daysMax = (int)$shippingModel->getConfigData($configPrefix . 'days_max');

        // get working day dates
        $today = new \DateTime();
        $dateMin = \Yasumi\Yasumi::nextWorkingDay($countryName, $today, $daysMin);
        $dateMax = \Yasumi\Yasumi::nextWorkingDay($countryName, $today, $daysMax);

        // format result
        $dateMinDay   = $dateMin->format('d');
        $dateMinMonth = $dateMin->format('m');
        $dateMinYear  = $dateMin->format('Y');

        $expectedDelivery = $dateMinDay != $dateMax->format('d') ? $dateMinDay .'.' : '';
        $expectedDelivery .= $dateMinMonth != $dateMax->format('m') ? $dateMinMonth .'.' : '';
        $expectedDelivery .= $dateMinYear != $dateMax->format('Y') ? $dateMinYear : '';

        $expectedDelivery .= !empty($expectedDelivery) ? ' - ' : '';
        $expectedDelivery .= $dateMax->format('d.m.Y');

        // add description
        return $expectedDelivery;
    }

}
