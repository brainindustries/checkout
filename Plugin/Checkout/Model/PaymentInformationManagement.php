<?php

namespace Shirtplatform\Checkout\Plugin\Checkout\Model;

class PaymentInformationManagement {

    /**
     * Set extension attributes on billing address
     * 
     * @access public
     * @param Magento\Checkout\Api\GuestPaymentInformationManagementInterface $subject
     * @param string $cartId
     * @param string $email
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param \Magento\Quote\Api\Data\AddressInterface $billingAddress
     * @return array
     */
    public function beforeSavePaymentInformation($subject,
                                                 $cartId,
                                                 $paymentMethod,
                                                 $billingAddress = null) {        
        if ($billingAddress) {
            $billingAddress->setBusinessNumber('');
            $billingAddress->setCareOf('');
            $billingAddress->setStreetNumber('');
            $extensionAttributes = $billingAddress->getExtensionAttributes();

            if ($extensionAttributes) {
                if (method_exists($extensionAttributes, 'getBusinessNumber')) {
                    $billingAddress->setBusinessNumber($extensionAttributes->getBusinessNumber());
                }

                if (method_exists($extensionAttributes, 'getCareOf')) {
                    $billingAddress->setCareOf($extensionAttributes->getCareOf());
                }

                if (method_exists($extensionAttributes, 'getStreetNumber')) {
                    $billingAddress->setStreetNumber($extensionAttributes->getStreetNumber());
                }                
            }
        }

        return [$cartId, $paymentMethod, $billingAddress];
    }

}
