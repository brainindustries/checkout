<?php

namespace Shirtplatform\Checkout\Plugin\Checkout\Model;

class ShippingInformationManagement {

    /**
     * @var \Magento\Quote\Model\QuoteRepository 
     */
    private $_quoteRepository;

    /**
     * 
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     */
    public function __construct(\Magento\Quote\Model\QuoteRepository $quoteRepository) {
        $this->_quoteRepository = $quoteRepository;
    }

    /**
     * Set extension attributes to quote address, set delivery branch information
     * to quote
     *      
     * @access public
     * @param \Magento\Checkout\Api\ShippingInformationManagementInterface $subject
     * @param int $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     * @return array
     */
    public function beforeSaveAddressInformation($subject,
                                                 $cartId,
                                                 \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation) {
        $address = $addressInformation->getShippingAddress();
        $carrierCode = $addressInformation->getShippingCarrierCode();
        $methodCode = $addressInformation->getShippingMethodCode();
        $extensionAttributes = $address->getExtensionAttributes();
        $quote = $this->_quoteRepository->getActive($cartId);        

        if ($carrierCode . '_' . $methodCode == \Shirtplatform\Shipping\Model\Carrier\Shipping::ZASIELKOVNA_METHOD_NAME) {
            $address->setCustomerAddressId(null);
        }

        $quote->setDeliveryBranchId(null);
        $quote->setDeliveryBranchName(null);
        $address->setPostNumber('');
        $address->setPackstationNumber('');
        $address->setBusinessNumber('');
        $address->setCareOf('');
        $address->setStreetNumber('');

        if ($extensionAttributes) {
            if (method_exists($extensionAttributes, 'getBusinessNumber')) {
                $address->setBusinessNumber($extensionAttributes->getBusinessNumber());
            }

            if (method_exists($extensionAttributes, 'getCareOf')) {
                $address->setCareOf($extensionAttributes->getCareOf());
            }

            if (method_exists($extensionAttributes, 'getStreetNumber')) {
                $address->setStreetNumber($extensionAttributes->getStreetNumber());
            }

            if ($extensionAttributes->getPostNumber()) {
                $address->setPostNumber($extensionAttributes->getPostNumber());
            }

            if ($extensionAttributes->getPackstationNumber()) {
                $address->setPackstationNumber($extensionAttributes->getPackstationNumber());
            }

            if ($address->getPostNumber() and $address->getPackstationNumber()) {
                $address->setCareOf($extensionAttributes->getPostNumber());
                $address->setStreetNumber($extensionAttributes->getPackstationNumber());
                $address->setStreet('Packstation');
            }
            
            if ($extensionAttributes->getDeliveryBranchId() != null) {
                $quote->setDeliveryBranchId($extensionAttributes->getDeliveryBranchId());
                $quote->setDeliveryBranchName($extensionAttributes->getDeliveryBranchName());
            }            
        }

        return [$cartId, $addressInformation];
    }

}
