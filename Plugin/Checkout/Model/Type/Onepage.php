<?php

namespace Shirtplatform\Checkout\Plugin\Checkout\Model\Type;

class Onepage {
    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;
    
    /**
     * 
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     */
    public function __construct(\Magento\Quote\Api\CartRepositoryInterface $quoteRepository) {
        $this->quoteRepository = $quoteRepository;
    }
    
    /**
     * Initialize quote state to be valid for one page checkout
     *
     * @access public
     * @param \Magento\Checkout\Model\Type\Onepage $subject
     * @param callable $proceed
     * @return $subject
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function aroundInitCheckout($subject, $proceed) {
        $checkout = $subject->getCheckout();
        $customerSession = $subject->getCustomerSession();
        if (is_array($checkout->getStepData())) {
            foreach ($checkout->getStepData() as $step => $data) {
                if (!($step === 'login' || $customerSession->isLoggedIn() && $step === 'billing')) {
                    $checkout->setStepData($step, 'allow', false);
                }
            }
        }

        $quote = $subject->getQuote();
        if ($quote->isMultipleShippingAddresses()) {
            $quote->removeAllAddresses();
            $this->quoteRepository->save($quote);
        }

        /*
         * want to load the correct customer information by assigning to address
         * instead of just loading from sales/quote_address
         * 
         * Change from the original class - set billing and shipping address if they
         * already exist. This caused problems for Packstation/Zasielkovna address when customer
         * was in billing step and refreshed the browser. Packstation/Zasielkovna address was
         * in local cache (and shown in summary shipping information), but in quote_address
         * table default shipping address was saved
         */
        $customer = $customerSession->getCustomerDataObject();
        if ($customer) {                                                
            $billingAddress = $quote->getBillingAddress();
            $shippingAddress = null;
            
            if (!$quote->isVirtual()) {
                $shippingAddress = $quote->getShippingAddress();
            }
            
            if ($billingAddress || $shippingAddress) {
                $quote->assignCustomerWithAddressChange($customer, $billingAddress, $shippingAddress);                
            }
            else {
                $quote->assignCustomer($customer);
            }
            
//            $quote->assignCustomer($customer);
        }
        return $subject;
    }
}