<?php

namespace Shirtplatform\Checkout\Plugin\Paypal\Model\Api;

class Nvp {
    /**
     *
     * @var \Magento\Checkout\Helper\Cart
     */
    private $_cartHelper;    

    /**
     * 
     * @param \Magento\Checkout\Helper\Cart $cartHelper     
     */
    public function __construct(\Magento\Checkout\Helper\Cart $cartHelper) {
        $this->_cartHelper = $cartHelper;
    }
    
    /**
     * Set shipping address for packstation and street_number as part
     * of street field
     * 
     * @access public
     * @param \Magento\Paypal\Model\Api\Nvp $subject
     * @return array
     */
    public function beforeCallSetExpressCheckout($subject) {
        $quote = $this->_cartHelper->getQuote();

        //Set shipping address for packstation
        if (!$quote->isVirtual()) {
            $quoteShippingAddress = $quote->getShippingAddress();

            if ($quoteShippingAddress->getPostNumber() and $quoteShippingAddress->getPackstationNumber()) {
                $quoteShippingAddress->setStreet('Packstation');
                $quoteShippingAddress->setStreetNumber($quoteShippingAddress->getPackstationNumber());
                $quoteShippingAddress->setCareOf($quoteShippingAddress->getPostNumber());
            }

            if (!$subject->getAddress() and true === $quoteShippingAddress->validate()) {
                $subject->setAddress($quoteShippingAddress);
            }
        }
        
        //Add street number as part of street for both addresses
        if ($subject->getAddress()) {
            $billingAddress = $subject->getBillingAddress() ? $subject->getBillingAddress() : $subject->getAddress();
            $street = $billingAddress->getStreet();
            if (is_array($street) and ! empty($street[0]) and $billingAddress->getStreetNumber()) {
                $street[0] .= ' ' . $billingAddress->getStreetNumber();                
                $billingAddress->setStreet($street);
            }

            $shippingAddress = $subject->getAddress();
            $street = $shippingAddress->getStreet();
            if (is_array($street) and ! empty($street[0]) and $shippingAddress->getStreetNumber()) {
                $street[0] .= ' ' . $shippingAddress->getStreetNumber();                
                $shippingAddress->setStreet($street);
            }
        }
        
        return [];
    }
}