<?php

namespace Shirtplatform\Checkout\Plugin\Paypal\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Checkout\Model\Session;

class Express
{
    /**
     * @var Session
     */
    private $_checkoutSession;

    /**
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @param Session $checkoutSession
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Session $checkoutSession,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @access public
     * @param Magento\Paypal\Model\Express $subject
     * @param bool $result
     * @return bool
     */
    public function afterIsAvailable($subject, $result) : bool
    {
        $quote = $this->_checkoutSession->getQuote();
        $maxPriceLimit = $this->_scopeConfig->getValue('payment/paypal_express/max_price_limit', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $quote->getStoreId());
        
        if (empty($maxPriceLimit)) {
            return $result;
        }
        
        return $quote->getGrandTotal() > $maxPriceLimit ? false : $result;
    }
}