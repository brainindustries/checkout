<?php

namespace Shirtplatform\Checkout\Plugin\Quote\Model;

use Shirtplatform\Core\Helper\Data as CoreHelper;
use shirtplatform\entity\order\Order;
use shirtplatform\filter\WsParameters;
use Magento\Framework\App\RequestInterface;

class Quote
{
    /**
     * @var CoreHelper
     */
    private $_coreHelper;

    /**
     * @var bool
     */
    private $_loggedOnShirtplatform = false;

    /**
     * @var RequestInterface
     */
    private $_request;

    /**
     * @param CoreHelper $coreHelper
     * @param RequestInterface $request
     */
    public function __construct(
        CoreHelper $coreHelper,
        RequestInterface $request
    ) {
        $this->_coreHelper = $coreHelper;
        $this->_request = $request;
    }

    public function aroundRemoveAllItems($subject, $proceed)
    {
        $platformOrderIds = [];

        foreach ($subject->getAllItems() as $_item) {
            $platformOrderId = $_item->getShirtplatformOrigOrderId();
            if (!empty($platformOrderId) && !in_array($platformOrderId, $platformOrderIds)) {
                $platformOrderIds[] = $platformOrderId;
            }
        }

        if (!count($platformOrderIds)) {
            return $proceed();
        }

        $result = $proceed();
        
        $this->_coreHelper->shirtplatformAuth($subject->getStoreId());

        $wsParams = new WsParameters();
        $wsParams->addExpressionContain('id', $platformOrderIds);
        $platformOrders = Order::findAll($wsParams);

        foreach ($platformOrders as $_order) {
            $platformOrderProducts = $_order->getOrderedProducts();
            if (is_array($platformOrderProducts)) {
                $_order->__delete();
            }
        }

        return $result;
    }
}