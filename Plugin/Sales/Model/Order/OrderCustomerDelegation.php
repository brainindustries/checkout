<?php

namespace Shirtplatform\Checkout\Plugin\Sales\Model\Order;

use Magento\Customer\Api\AccountDelegationInterface;
use Magento\Framework\Controller\Result\Redirect;

class OrderCustomerDelegation
{
    /**
     * @var \Magento\Sales\Model\Order\OrderCustomerExtractor
     */
    private $_customerExtractor;

    /**
     * @var AccountDelegationInterface
     */
    private $_delegateService;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $_session;

    /**     
     * @access public
     * @param \Magento\Sales\Model\Order\OrderCustomerExtractor $customerExtractor
     * @param AccountDelegationInterface $delegateService
     * @param \Magento\Customer\Model\Session $session
     * @return void
     */
    public function __construct(
        \Magento\Customer\Model\Session $session,
        \Magento\Sales\Model\Order\OrderCustomerExtractor $customerExtractor,
        AccountDelegationInterface $delegateService
    ) {
        $this->_session = $session;
        $this->_customerExtractor = $customerExtractor;
        $this->_delegateService = $delegateService;
    }
    
    /**
     * Remove care_of, street_number, business_number from session as they're causing problems
     * when registering customer and they're stored on order_address
     *
     * @access public
     * @param \Magento\Sales\Api\OrderCustomerDelegateInterface $subject
     * @param \Closure $proceed
     * @param int $orderId
     * @return Redirect
     */
    public function aroundDelegateNew($subject, $proceed, int $orderId): Redirect
    {
        $redirect = $this->_delegateService->createRedirectForNew(
            $this->_customerExtractor->extract($orderId),
            ['__sales_assign_order_id' => $orderId]
        );

        $delegatedNewCustomerData = $this->_session->getDelegatedNewCustomerData();
        if (isset($delegatedNewCustomerData['customer']['addresses'])) {
            foreach ($delegatedNewCustomerData['customer']['addresses'] as $key => $address) {
                if (isset($address['custom_attributes']['care_of'])) {
                    unset($delegatedNewCustomerData['customer']['addresses'][$key]['custom_attributes']['care_of']);
                }
                if (isset($address['custom_attributes']['street_number'])) {
                    unset($delegatedNewCustomerData['customer']['addresses'][$key]['custom_attributes']['street_number']);
                }
                if (isset($address['custom_attributes']['business_number'])) {
                    unset($delegatedNewCustomerData['customer']['addresses'][$key]['custom_attributes']['business_number']);
                }
            }
        }

        if (isset($delegatedNewCustomerData['addresses'])) {
            foreach ($delegatedNewCustomerData['addresses'] as $key => $address) {
                if (isset($address['custom_attributes']['care_of'])) {
                    unset($delegatedNewCustomerData['addresses'][$key]['custom_attributes']['care_of']);
                }
                if (isset($address['custom_attributes']['street_number'])) {
                    unset($delegatedNewCustomerData['addresses'][$key]['custom_attributes']['street_number']);
                }
                if (isset($address['custom_attributes']['business_number'])) {
                    unset($delegatedNewCustomerData['addresses'][$key]['custom_attributes']['business_number']);
                }
            }
        }
        
        $this->_session->setDelegatedNewCustomerData($delegatedNewCustomerData);
        return $redirect;
    }
}
