<?php

namespace Shirtplatform\Checkout\Plugin\Tax\Model\Sales\Total\Quote;

class CommonTaxCollector
{    
    /**
     * Set custom_price back to what it was if the difference is more than 0.001. The reason is
     * that platform sends price rounded on 3 decimal digits, Magento rounds it on 2 and in 
     * the original method the custom_price is set to price (rounded on 2 digits). 
     * Reorder, in some cases, produces wrong tax calculations - 1 cent difference
     *
     * @access public
     * @param \Magento\Tax\Model\Sales\Total\Quote\CommonTaxCollector $subject
     * @param \Closure
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $quoteItem
     * @param \Magento\Tax\Api\Data\TaxDetailsItemInterface $itemTaxDetails
     * @param \Magento\Tax\Api\Data\TaxDetailsItemInterface $baseItemTaxDetails
     * @param \Magento\Store\Model\Store $store
     * @return \Magento\Tax\Model\Sales\Total\Quote\CommonTaxCollector
     */
    public function aroundUpdateItemTaxInfo($subject, $proceed, $quoteItem, $itemTaxDetails, $baseItemTaxDetails, $store)
    {
        $originalCustomPrice = $quoteItem->getCustomPrice();
        $result = $proceed($quoteItem, $itemTaxDetails, $baseItemTaxDetails, $store);
        
        if ($originalCustomPrice && abs($originalCustomPrice - $quoteItem->getCustomPrice()) > 0.001) {
            $quoteItem->setCustomPrice($originalCustomPrice);
        }
        return $result;
    }
}