<?php

namespace Shirtplatform\Checkout\Plugin\Tax\Model;

class TaxConfigProvider {

    /**
     * @var \Magento\Customer\Helper\Address
     */
    protected $_customerAddress;    

    /**
     * 
     * @param \Magento\Customer\Helper\Address $customerAddress     
     */
    public function __construct(\Magento\Customer\Helper\Address $customerAddress) {
        $this->_customerAddress = $customerAddress;        
    }

    /**     
     * 
     * @access public
     * @param \Magento\Tax\Model\TaxConfigProvider $subject
     * @param array $result
     * @return array
     */
    public function afterGetConfig($subject,
                                   $result) {        
        if (
                $this->_customerAddress->isVatValidationEnabled() and
                $this->_customerAddress->getTaxCalculationAddressType() == \Magento\Customer\Model\Address\AbstractAddress::TYPE_BILLING
        ) {
            //force reloadOnBillingAddress if VAT validation is based on billing address
            $result['reloadOnBillingAddress'] = true;
        }
        
        return $result;
    }

}
