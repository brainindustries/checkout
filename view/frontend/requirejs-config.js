var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/sidebar': {
                'Shirtplatform_Checkout/js/sidebar-mixin': true
            },
            'Magento_Checkout/js/action/select-shipping-method': {
                'Shirtplatform_Checkout/js/action/select-shipping-method-mixin': true
            },
            'Magento_Checkout/js/model/checkout-data-resolver': {
                'Shirtplatform_Checkout/js/model/checkout-data-resolver-mixin': true
            },
            'Magento_Checkout/js/model/new-customer-address': {
                'Shirtplatform_Checkout/js/model/new-customer-address-mixin': true
            },
            'Magento_Checkout/js/model/quote': {
                'Shirtplatform_Checkout/js/model/quote-mixin': true
            },
            'Magento_Checkout/js/model/shipping-save-processor/payload-extender': {
                'Shirtplatform_Checkout/js/model/shipping-save-processor/payload-extender-mixin': true
            },
            'Magento_Checkout/js/model/shipping-service': {
                'Shirtplatform_Checkout/js/model/shipping-service-mixin': true
            },
            'Magento_Checkout/js/view/billing-address': {
                'Shirtplatform_Checkout/js/view/billing-address-mixin': true
            },
            'Magento_Checkout/js/view/payment/default': {
                'Shirtplatform_Checkout/js/view/payment/default-mixin': true
            },
            'Magento_Checkout/js/view/form/element/email': {
                'Shirtplatform_Checkout/js/view/form/element/email-mixin': true
            },
            'Magento_Checkout/js/view/shipping': {
                'Shirtplatform_Checkout/js/view/shipping-mixin': true
            },
            'Magento_Checkout/js/view/shipping-address/address-renderer/default': {
                'Shirtplatform_Checkout/js/view/shipping-address/address-renderer/default-mixin': true
            },
            'Magento_Checkout/js/view/shipping-information/address-renderer/default': {
                'Shirtplatform_Checkout/js/view/shipping-information/address-renderer/default-mixin': true
            },
            'Magento_Customer/js/model/customer/address': {
                'Shirtplatform_Checkout/js/model/customer/address-mixin': true
            },
            'Magento_Paypal/js/view/payment/paypal-payments': {
                'Shirtplatform_Checkout/js/view/payment/paypal-payments-mixin': true
            },
            'Temando_Shipping/js/view/checkout/shipping-information/address-renderer/shipping': {
                'Shirtplatform_Checkout/js/view/shipping-information/address-renderer/default-mixin': true
            }
        }
    }
};
