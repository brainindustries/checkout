define([
    'uiRegistry',
    'mage/utils/wrapper',
    'Magento_Checkout/js/checkout-data',
    'Shirtplatform_Checkout/js/checkout-data'
], function (uiRegistry, wrapper, checkoutData, shirtplatformCheckoutData) {
    'use strict';

    return function (selectShippingMethodFunction) {
        return wrapper.wrap(selectShippingMethodFunction, function (originalSelectShippingMethodFunction, shippingMethod) {

            let isBillingAddressSameAsShipping = null;
            let canUseShippingAddressForBilling = null;

            uiRegistry.async('checkout.steps.shipping-step.shippingAddress.billingAddress')(function (billingAddress) {
                isBillingAddressSameAsShipping = billingAddress.isAddressSameAsShipping();
                canUseShippingAddressForBilling = billingAddress.canUseShippingAddress();
            });

            originalSelectShippingMethodFunction(shippingMethod);

            if (!shippingMethod) {
                return;
            }

            let methodName = shippingMethod['carrier_code'] + '_' + shippingMethod['method_code'];

            if (methodName == window.checkoutConfig.zasielkovna_method_name) {
                uiRegistry.async('checkout.steps.shipping-step.shippingAddress')(function (shippingAddress) {
                    // display zasielkovna map
                    if (!shirtplatformCheckoutData.getDeliveryBranchId()) {
                        Packeta.Widget.pick(
                            window.checkoutConfig.zasielkovna_key,                            
                            shippingAddress.showSelectedBranch,
                            { 
                                country: shippingAddress.country,
                                language: window.checkoutConfig.locale.substring(0, window.checkoutConfig.locale.indexOf('_'))
                            }
                        );
                    }
                });
            }

            if (methodName == window.checkoutConfig.zasielkovna_method_name || methodName == window.checkoutConfig.packetery_method_name) {
                // copy shipping address to billing address
                let shippingAddress = checkoutData.getShippingAddressFromData();

                if (!shippingAddress || !isBillingAddressSameAsShipping || !canUseShippingAddressForBilling) {
                    return;
                }

                uiRegistry.async('checkout.steps.shipping-step.shippingAddress.billingAddress.address-fieldset')(function (fieldset) {
                    for (let i in fieldset.elems()) {
                        let elem = fieldset.elems()[i];
                        let pathSegments = elem.name.split('.');
                        let name = pathSegments[pathSegments.length - 1];

                        if (name != 'street') {
                            if (shippingAddress[name]) {
                                elem.value(shippingAddress[name]);
                            }
                        } else {
                            for (let j in elem.elems()) {
                                if (shippingAddress[name][j]) {
                                    elem.elems()[j].value(shippingAddress[name][j]);
                                }
                            }
                        }
                    }
                });

                checkoutData.setBillingAddressFromData(shippingAddress);
            }
        });
    };
});
