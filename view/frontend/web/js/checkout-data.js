define([
    'jquery',
    'Magento_Customer/js/customer-data'
], function($, storage) {
    'use strict';
    
    var cacheKey = 'shirtplatform-checkout-data',
            /**
             * @param {Object} data
             */
            saveData = function (data) {
                storage.set(cacheKey, data);
            },
            /**
             * @return {*}
             */
            getData = function () {
                var data = storage.get(cacheKey)();

                if ($.isEmptyObject(data)) {
                    data = {
                        isPackstation: false,
                        deliveryBranchId: null,
                        deliveryBranchName: null,
                        zasielkovnaAddress: null,
                        customerNote: null,
                        newsletterSubscribeChecked: null
                    };
                    saveData(data);
                }

                return data;
            };
    
    return {
        
        /**
         * Get Zasielkovna delivery_branch_id
         * 
         * @returns {int}
         */
        getDeliveryBranchId: function() {
            return getData().deliveryBranchId;
        },
        
        /**
         * Set Zasielkovna delivery_branch_Id
         * 
         * @param {int} deliveryBranchId         
         */
        setDeliveryBranchId: function(deliveryBranchId) {
            var obj = getData();
            obj.deliveryBranchId = deliveryBranchId;
            saveData(obj);
        },
        
        /**
         * Get Zasielkovna delivery_branch_name
         * 
         * @returns {int}
         */
        getDeliveryBranchName: function() {
            return getData().deliveryBranchName;
        },
        
        /**
         * Set Zasielkovna delivery_branch_name
         * 
         * @param {String} deliveryBranchName         
         */
        setDeliveryBranchName: function(deliveryBranchName) {
            var obj = getData();
            obj.deliveryBranchName = deliveryBranchName;
            saveData(obj);
        },
        
        /**
         * Get Zasielkovna delivery_branch_type
         * 
         * @returns {int}
         */
        getDeliveryBranchType: function() {
            return getData().deliveryBranchType;
        },        
        
        /**
         * Set Zasielkovna delivery_branch_type
         * 
         * @param {String} deliveryBranchType         
         */
        setDeliveryBranchType: function(deliveryBranchType) {
            var obj = getData();
            obj.deliveryBranchType = deliveryBranchType;
            saveData(obj);
        },
        
        /**
         * Is packstation?
         * 
         * @returns {Boolean}
         */
        getIsPackstation: function() {
            return getData().isPackstation;
        },
        
        /**
         * Set isPackstation property
         * 
         * @param {Boolean} isPackstation         
         */
        setIsPackstation: function(isPackstation) {
            var obj = getData();
            obj.isPackstation = isPackstation;
            saveData(obj);
        },
        
        /**
         * Get address of zasielkovna branch
         * 
         * @returns {Object}
         */
        getZasielkovnaAddress: function() {
            return getData().zasielkovnaAddress;
        },
        
        /**
         * Set address of zasielkovna branch
         * 
         * @param {Object} data         
         */
        setZasielkovnaAddress: function(data) {
            var obj = getData();
            obj.zasielkovnaAddress = data;
            saveData(obj);
        },
        
        /**
         * Get customer note
         * 
         * @returns {Object}
         */
        getCustomerNote: function() {
            return getData().customerNote;
        },
        
        /**
         * Set customer note
         * 
         * @param {Object} data         
         */
        setCustomerNote: function(data) {
            var obj = getData();
            obj.customerNote = data;
            saveData(obj);
        },

        /**
         * Get value of newsletterSubscribeChecked
         * 
         * @returns {int}
         */
         getNewsletterSubscribeChecked: function() {
            return getData().newsletterSubscribeChecked;
        },

        /**
         * Set the state of newsletterSubscribeChecked checkbox
         * 
         * @param {Object} data         
         */
        setNewsletterSubscribeChecked: function(isChecked) {
            var obj = getData();
            obj.newsletterSubscribeChecked = isChecked;
            saveData(obj);
        }
    }
});