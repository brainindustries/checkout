define([
    'Magento_Customer/js/model/address-list',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/action/create-billing-address',
    'Magento_Checkout/js/action/select-billing-address',
    'Magento_Checkout/js/action/select-shipping-address',
    'mage/utils/wrapper',
    'Shirtplatform_Checkout/js/checkout-data'
], function (addressList, quote, checkoutData, addressConverter, createBillingAddress, selectBillingAddress, selectShippingAddress, wrapper, shirtplatformCheckoutData) {
    'use strict';

    return function (checkoutDataResolver) {
        /**
         * Apply resolved estimated address to quote. Resolve zasielkovna address
         *
         * @param {Object} isEstimatedAddress
         */
        checkoutDataResolver.applyShippingAddress = wrapper.wrapSuper(checkoutDataResolver.applyShippingAddress, function (isEstimatedAddress) {
            var address,
                    shippingAddress,
                    isConvertAddress,
                    addressData,
                    isShippingAddressInitialized;

            var shippingMethod = checkoutData.getSelectedShippingRate()
            var zasielkovnaAddress = null;

            if (shippingMethod === window.checkoutConfig.packetery_method_name){
                window.packetaPoint.name = shirtplatformCheckoutData.getDeliveryBranchName();
                window.packetaPoint.pickupPointType = shirtplatformCheckoutData.getDeliveryBranchType();
                window.packetaPoint.pointId = shirtplatformCheckoutData.getDeliveryBranchId();
            }

            if (shippingMethod === window.checkoutConfig.zasielkovna_method_name || shippingMethod === window.checkoutConfig.packetery_method_name) {
                zasielkovnaAddress = shirtplatformCheckoutData.getZasielkovnaAddress();

                if (zasielkovnaAddress) {
                    address = addressConverter.formAddressDataToQuoteAddress(zasielkovnaAddress);
                    selectShippingAddress(address);
                }
            }

            if (addressList().length === 0 && !address) {
                address = addressConverter.formAddressDataToQuoteAddress(
                        checkoutData.getShippingAddressFromData()
                        );

                selectShippingAddress(address);
            }
            shippingAddress = quote.shippingAddress();
            isConvertAddress = isEstimatedAddress || false;

            if (!shippingAddress) {
                isShippingAddressInitialized = addressList.some(function (addressFromList) {
                    if (checkoutData.getSelectedShippingAddress() == addressFromList.getKey()) { //eslint-disable-line
                        addressData = isConvertAddress ?
                                addressConverter.addressToEstimationAddress(addressFromList)
                                : addressFromList;
                        selectShippingAddress(addressData);
                        checkoutData.setSelectedShippingAddress(addressData.getKey());

                        return true;
                    }

                    return false;
                });

                if (!isShippingAddressInitialized) {
                    isShippingAddressInitialized = addressList.some(function (addrs) {
                        if (addrs.isDefaultShipping()) {
                            addressData = isConvertAddress ?
                                    addressConverter.addressToEstimationAddress(addrs)
                                    : addrs;
                            selectShippingAddress(addressData);
                            checkoutData.setSelectedShippingAddress(addressData.getKey());

                            return true;
                        }

                        return false;
                    });
                }

                if (!isShippingAddressInitialized && addressList().length === 1) {
                    addressData = isConvertAddress ?
                            addressConverter.addressToEstimationAddress(addressList()[0])
                            : addressList()[0];
                    selectShippingAddress(addressData);
                    checkoutData.setSelectedShippingAddress(addressData.getKey());
                }
            }
        });

        /**
         * Apply resolved billing address to quote
         */
        checkoutDataResolver.applyBillingAddress = wrapper.wrapSuper(checkoutDataResolver.applyBillingAddress, function () {
            var shippingAddress,
                    isBillingAddressInitialized;

            if (quote.billingAddress()) {
                selectBillingAddress(quote.billingAddress());

                return;
            }

            if (quote.isVirtual() || !quote.billingAddress()) {
                isBillingAddressInitialized = addressList.some(function (addrs) {
                    if (addrs.isDefaultBilling()) {
                        selectBillingAddress(addrs);

                        return true;
                    }

                    return false;
                });
            }

            shippingAddress = quote.shippingAddress();

            if (!isBillingAddressInitialized &&
                    shippingAddress &&
                    shippingAddress.canUseForBilling() &&
                    (shippingAddress.isDefaultShipping() || !quote.isVirtual())
                    ) {                
                selectBillingAddress(quote.shippingAddress());
                return;
            }
            
            //in case shippingAddress can't be used for billing - initialize it from the form
            var formBillingAddress = checkoutData.getBillingAddressFromData();
            if (!quote.billingAddress() && formBillingAddress) {
                var address = createBillingAddress(formBillingAddress);
                selectBillingAddress(address);
            }
        });

        /**
         * Set selected shipping address as quote.shippingAddress()
         */
        checkoutDataResolver.applySelectedShippingAddress = function() {
            addressList.some(function (addressFromList) {
                if (checkoutData.getSelectedShippingAddress() == addressFromList.getKey()) {                    
                    selectShippingAddress(addressFromList);                    
                }                    
            });            
        };
        
        return checkoutDataResolver;
    }
});