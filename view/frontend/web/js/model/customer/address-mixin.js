define([    
    'mage/utils/wrapper',    
], function (wrapper) {
    'use strict';
    
    return function (customerAddress) {
        return wrapper.wrap(customerAddress, function (originalFunction, addressData) {
            var result = originalFunction(addressData);
            
            if (result.extension_attributes === undefined) {
                result.extension_attributes = {};
            }
            
            if (addressData.business_number != undefined) {
                result.extension_attributes.business_number = addressData.business_number;
            }
            else if (addressData.custom_attributes != undefined && addressData.custom_attributes.business_number != undefined) {
                result.extension_attributes.business_number = addressData.custom_attributes.business_number.value;
            }
            
            if (addressData.street_number != undefined) {
                result.extension_attributes.street_number = addressData.street_number;
            }
            else if (addressData.custom_attributes != undefined && addressData.custom_attributes.street_number != undefined) {
                result.extension_attributes.street_number = addressData.custom_attributes.street_number.value;
            }
            
            if (addressData.care_of != undefined) {
                result.extension_attributes.care_of = addressData.care_of;
            }
            else if (addressData.custom_attributes != undefined && addressData.custom_attributes.care_of != undefined) {
                result.extension_attributes.care_of = addressData.custom_attributes.care_of.value;
            }
            
            return result;
        });
    }
});