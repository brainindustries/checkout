define([    
    'mage/utils/wrapper',
    'Shirtplatform_Checkout/js/checkout-data'
], function (wrapper, shirtplatformCheckoutData) {
    'use strict';
    
    return function (newCustomerAddress) {
        return wrapper.wrap(newCustomerAddress, function (originalFunction, addressData) {
            var result = originalFunction(addressData);

            if (result.extension_attributes === undefined) {
                result.extension_attributes = {};
            }

            if (addressData.business_number != undefined) {
                result.extension_attributes.business_number = addressData.business_number;
            }
            else if (addressData.extension_attributes && addressData.extension_attributes.business_number != undefined) {
                result.extension_attributes.business_number = addressData.extension_attributes.business_number;
            }

            if (addressData.post_number != undefined) {
                result.extension_attributes.post_number = addressData.post_number;
            }
            else if (addressData.extension_attributes && addressData.extension_attributes.post_number != undefined) {
                result.extension_attributes.post_number = addressData.extension_attributes.post_number;
            }

            if (addressData.packstation_number != undefined) {
                result.extension_attributes.packstation_number = addressData.packstation_number;
            }
            else if (addressData.extension_attributes && addressData.extension_attributes.packstation_number != undefined) {
                result.extension_attributes.packstation_number = addressData.extension_attributes.packstation_number;
            }         
            
            if (addressData.street_number != undefined) {
                result.extension_attributes.street_number = addressData.street_number;
            }
            else if (addressData.extension_attributes && addressData.extension_attributes.street_number != undefined) {
                result.extension_attributes.street_number = addressData.extension_attributes.street_number;
            }
            
            if (addressData.care_of != undefined) {
                result.extension_attributes.care_of = addressData.care_of;
            }
            else if (addressData.extension_attributes && addressData.extension_attributes.care_of != undefined) {
                result.extension_attributes.care_of = addressData.extension_attributes.care_of;
            }
                        
            result.canUseForBilling = function() {
                if (this.street && this.street.length && this.street[0] == 'Packstation') {
                    return false;
                }                               
                
                if (this.extension_attributes['packstation_number'] || this.extension_attributes['post_number']) {
                    return false;
                }
                
                if (shirtplatformCheckoutData.getDeliveryBranchId()) {
                    var zasielkovnaAddress = shirtplatformCheckoutData.getZasielkovnaAddress();
                                        
                    if (zasielkovnaAddress == null) {
                        return true;
                    } 
                    
                    if (this.extension_attributes['care_of'] != zasielkovnaAddress.extension_attributes['care_of']) {
                        return true;
                    }
                    if (this.street && this.street[0] != zasielkovnaAddress.street[0]) {
                        return true;
                    }
                    if (this.extension_attributes['street_number'] != zasielkovnaAddress.extension_attributes['street_number']) {
                        return true;
                    }                                                            
                    
                    return false;
                }
                
                return true;
            }
                        
            return result;
        });
    }
});