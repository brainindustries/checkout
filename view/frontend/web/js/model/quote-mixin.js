define([
    'mage/utils/wrapper',
    'uiRegistry'
], function(wrapper, uiRegistry) {
    'use strict';

    return function(quote) {
        /**
         * Checks if the billing address has all the needed values to be considered complete
         * 
         * @returns {Boolean}
         */
        quote.isBillingAddressComplete = function() {            
            let requiredFields = ['firstname', 'lastname', 'city', 'postcode', 'countryId'];
            let isAddressComplete = true;
            let address = this.billingAddress();

            for (let field of requiredFields) {
                if (!address[field]) {
                    isAddressComplete = false;
                }
            }

            if (!address['street']) {
                isAddressComplete = false;
            }
            else {
                let streetNotEmpty = false;
                for (let line in address['street']) {
                    if (address['street'][line].trim() !== '') {
                        streetNotEmpty = true;
                        break;
                    }
                }

                if (!streetNotEmpty) {
                    isAddressComplete = false;
                }
            }

            let allStreetNumbers = uiRegistry.filter('index = street_number');
            if (allStreetNumbers) {
                //we can get the first street_number field as all of them are required or not
                let streetNumber = allStreetNumbers[0];

                if (streetNumber.required() && (!address['extension_attributes'] || (address['extension_attributes'] && !address['extension_attributes']['street_number']))) {
                    isAddressComplete = false;
                }            
            }

            return isAddressComplete;
        }

        return quote;
    }
});