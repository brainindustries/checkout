define([
    'jquery',
    'uiRegistry',
    'mage/utils/wrapper',    
    'Magento_Customer/js/model/customer',
    'Shirtplatform_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/quote'
], function($, uiRegistry, wrapper, customer, shirtplatformCheckoutData, quote) {
    'use strict';
    
    return function(payloadExtender) {
        return wrapper.wrap(payloadExtender, function(originalAction, payload) {
            var payload = originalAction(payload);
            var shippingAddress = payload.addressInformation['shipping_address'];
            
            if (shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
            }
            
            //add packstation data
            if (window.checkoutConfig.usePackstation == 1) {
                shippingAddress['extension_attributes']['post_number'] = '';
                shippingAddress['extension_attributes']['packstation_number'] = '';
                
                if (shirtplatformCheckoutData.getIsPackstation()) {
                    var postNumber = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.post_number');
                    var packstationNumber = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.packstation_number');
                    
                    if (postNumber) {
                        shippingAddress['extension_attributes']['post_number'] = postNumber.value();
                    }
                    if (packstationNumber) {
                        shippingAddress['extension_attributes']['packstation_number'] = packstationNumber.value();
                    }
                }
            }
            
            //add zasielkovna data
            var shippingMethodName = payload.addressInformation['shipping_carrier_code'] + '_' + payload.addressInformation['shipping_method_code'];
            shippingAddress['extension_attributes']['delivery_branch_id'] = '';
            shippingAddress['extension_attributes']['delivery_branch_name'] = '';
            
            if (shippingMethodName == window.checkoutConfig.zasielkovna_method_name || shippingMethodName == window.checkoutConfig.packetery_method_name) {
                var zasielkovnaAddress = shirtplatformCheckoutData.getZasielkovnaAddress(); 
                
                if (customer.isLoggedIn()) {
                    //this is important, otherwise saved customer address will be overwritten in billing step when payment method changed
                    var payloadShippingAddress = $.extend(true, {}, shippingAddress);
                    payloadShippingAddress.save_in_address_book = 0;
                }
                else {
                    var payloadShippingAddress = shippingAddress;
                }                                              
                
                payloadShippingAddress.street = zasielkovnaAddress.street;
                payloadShippingAddress.city = zasielkovnaAddress.city;
                payloadShippingAddress.postcode = zasielkovnaAddress.postcode;
                payloadShippingAddress.countryId = zasielkovnaAddress.countryId;                
                payloadShippingAddress['extension_attributes']['care_of'] = zasielkovnaAddress['extension_attributes']['care_of'];
                payloadShippingAddress['extension_attributes']['street_number'] = zasielkovnaAddress['extension_attributes']['street_number'];
                payloadShippingAddress['extension_attributes']['delivery_branch_id'] = shirtplatformCheckoutData.getDeliveryBranchId();
                payloadShippingAddress['extension_attributes']['delivery_branch_name'] = shirtplatformCheckoutData.getDeliveryBranchName();                
                payload.addressInformation['shipping_address'] = payloadShippingAddress;
            }

            if (!customer.isLoggedIn()){
                payload.addressInformation['shipping_address']['email'] = quote.guestEmail;
            }

            return payload;
        });
    }
});