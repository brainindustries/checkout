var SHIPCLOUD_DHL_STANDARD_CODE = 1;
var DEPOST_CODE = 5;

define([
    'mage/utils/wrapper',    
    'Magento_Checkout/js/model/quote',
    'Shirtplatform_Checkout/js/checkout-data'
], function (wrapper, quote, shirtplatformCheckoutData) {
    'use strict';

    return function (shippingService) {
        /**
         * Set shipping rates - in case of Packstation, only DHL Standard and Magento 
         * rates are allowed; if freeshipping is enabled, use freeshipping title and 0 price
         * for GLS method and hide original freeshipping method
         */
        shippingService.setShippingRates = wrapper.wrapSuper(shippingService.setShippingRates, function (ratesData) {
            var resultRates = [];

            var subtotal = parseInt(quote.totals().subtotal_with_discount);
            var minSubtotal = parseInt(window.checkoutConfig.freeshippingSubtotalMin);

            for(var i in ratesData) {
                var methodCode = parseInt(ratesData[i]['method_code']);

                if (ratesData[i]['carrier_code'] != 'shirtplatformShipping') {
                    resultRates.push(ratesData[i]);
                    continue;
                }

                if (window.checkoutConfig.isFreeshippingEnabled && methodCode == window.checkoutConfig.freeshippingCarrier && subtotal >= minSubtotal){
                    continue;
                }

                if (shirtplatformCheckoutData.getIsPackstation()){
                    if (methodCode != SHIPCLOUD_DHL_STANDARD_CODE){
                        continue;
                    }
                } else if (window.checkoutConfig.isAdditionalShippingLogicEnabled && (methodCode == SHIPCLOUD_DHL_STANDARD_CODE || methodCode == DEPOST_CODE)){
                    if (!ratesData[i].extension_attributes.used_in_additional_logic){
                        continue;
                    } else {
                        ratesData[i]['amount'] = window.checkoutConfig.additionalShippingData['price_excl_tax'];
                        ratesData[i]['base_amount'] = window.checkoutConfig.additionalShippingData['price_excl_tax'];
                        ratesData[i]['price_excl_tax'] = window.checkoutConfig.additionalShippingData['price_excl_tax'];
                        ratesData[i]['price_incl_tax'] = window.checkoutConfig.additionalShippingData['price_incl_tax'];
                        ratesData[i]['carrier_title'] = window.checkoutConfig.additionalShippingData['title_' + methodCode];
                        ratesData[i]['method_title'] = window.checkoutConfig.additionalShippingData['name_' + methodCode];
                    }
                }

                resultRates.push(ratesData[i]);
            }

            this._super(resultRates);
        });
        
        return shippingService;
    }
});