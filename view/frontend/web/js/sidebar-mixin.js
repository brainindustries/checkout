define([
    'jquery',
    'underscore'
], function($, _) {
    'use strict';

    var sidebarMixin = {
        /**
         * Update content after item remove
         *
         * @param {Object} elem
         * @private
         */
        _removeItemAfter: function (elem) {
            var productData = this._getProductById(Number(elem.data('cart-item')));

            if (!_.isUndefined(productData)) {
                $(document).trigger('ajax:removeFromCart', {
                    productIds: [productData['product_id']],
                    productInfo: [
                        {
                            'id': productData['product_id']
                        }
                    ]
                });
                
                //don't reload if we are editing product in creator
                if (window.location.href.indexOf(this.shoppingCartUrl) === 0 && !/\/checkout\/cart\/configure/.test(window.location.href)) {
                    window.location.reload();
                }
            }
        }
    };

    return function (sidebar) {
        $.widget('mage.sidebar', sidebar, sidebarMixin);
        return $.mage.sidebar;
    }
});