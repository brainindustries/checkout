define([
    'ko',
    'uiRegistry',
    'Magento_Checkout/js/model/quote',
    'Shirtplatform_Checkout/js/checkout-data'
], function (
        ko,
        uiRegistry,
        quote,
        shirtplatformCheckoutData) {
    'use strict';
    return function (billingAddress) {
        return billingAddress.extend({
            addressDetailsVisibleValue: ko.observable(false),
            defaults: {
                actionsTemplate: 'Shirtplatform_Checkout/billing-address/actions',
                detailsTemplate: 'Shirtplatform_Checkout/billing-address/details',
            },
            initObservable: function () {
                var self = this;
                this._super();
                                
                self.isAddressDetailsVisible.subscribe(function(isVisible) {                                        
                    if (isVisible && self.isPostBoxAddress()) {
                        self.isAddressDetailsVisible(false);                        
                    }                                        
                });

                this.canUseShippingAddress = ko.computed(function () {
                    if (quote.isVirtual() || self.isPostBoxAddress()) {
                        self.isAddressSameAsShipping(false);
                        self.isAddressDetailsVisible(false);
                        return false;
                    }

                    return quote.shippingAddress() && quote.shippingAddress().canUseForBilling();
                });                                

                //go through all billing address forms and set validation rules for business_number based on vat_id
                if (window.checkoutConfig.isBusinessNumberRequiredForVatId){
                uiRegistry.async('parentScope = ' + this.dataScopePrefix + ', index = vat_id')(
                        function (vatId) {
                            self.setBusinessNumberValidationRule(vatId.value());
                            vatId.value.subscribe(function (newValue) {
                                self.setBusinessNumberValidationRule(newValue);
                            });
                        });
                }
                        
                return this;
            },
            /**
             * Is post box address chosen (Zasielkovna or Packstation)
             * 
             * @returns {Boolean}
             */
            isPostBoxAddress: function () {
                var method = quote.shippingMethod();
                var methodName = method != null ? method.carrier_code + '_' + method.method_code : null;
                if (methodName === window.checkoutConfig.zasielkovna_method_name || methodName === window.checkoutConfig.packetery_method_name) {
                    return true;
                }
                
                if (shirtplatformCheckoutData.getIsPackstation()) {
                    return true;
                }

                return false;
            },

            /**
             * Set validation rule for business_number based on vat_id value
             * 
             * @param {String} vatIdValue             
             */
            setBusinessNumberValidationRule: function (vatIdValue) {
                var businessNumber = uiRegistry.get('parentScope = ' + this.dataScopePrefix + ', index = business_number');

                if (businessNumber && vatIdValue.trim() != '') {
                    businessNumber.validation['required-entry'] = true;
                    businessNumber.required(true);
                } else {
                    businessNumber.validation['required-entry'] = false;
                    businessNumber.required(false);
                    businessNumber.validate();
                }
            }
        });
    };
});