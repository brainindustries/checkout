define(['Magento_Checkout/js/checkout-data'], function (checkoutData) {
    'use strict';

    var mixin = {
        resolveInitialPasswordVisibility: function () {
            return checkoutData.getInputFieldEmailValue() !== '' && checkoutData.getCheckedEmailValue() !== ''
                ? checkoutData.getInputFieldEmailValue() === checkoutData.getCheckedEmailValue()
                : false;
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
