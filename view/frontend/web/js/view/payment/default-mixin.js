define([
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/customer-data',    
    'uiRegistry'
], function (addressConverter, quote, customerData, uiRegistry) {
    'use strict';

    return function (parent) {
        return parent.extend({
            initialize: function() {                                
                this._super();

                /**
                 * Place order button can be enabled only if there is enough data in billing address
                 */
                this.isPlaceOrderActionAllowed.subscribe(function(isAllowed) {                    
                    if (isAllowed && !quote.isBillingAddressComplete()) {
                        this.isPlaceOrderActionAllowed(false);
                    }
                }, this);

                if (!quote.isBillingAddressComplete()) {
                    this.isPlaceOrderActionAllowed(false);
                }                
            },

            afterPlaceOrder: function () {
                this._super();                
                var clearData = {
                    isPackstation: false,
                    deliveryBranchId: null,
                    deliveryBranchName: null,
                    zasielkovnaAddress: null,
                    customerNote: null,
                    newsletterSubscribeChecked: null
                };

                customerData.set('shirtplatform-checkout-data', clearData);
            },

            placeOrder: function(data, event) { 
                let billingAddresses;

                if (window.checkoutConfig.displayBillingOnPaymentMethod) {
                    billingAddresses = uiRegistry.filter('dataScopePrefix = billingAddress' + data.index);                
                }
                else {
                    billingAddresses = uiRegistry.filter('dataScopePrefix = billingAddressshared');
                }
                    
                if (billingAddresses.length) {
                    let billingAddress = billingAddresses[0];

                    if (billingAddress.isAddressFormVisible()) {
                        let formData = billingAddress.source.get(billingAddress.dataScopePrefix);
                        let quoteAddress = addressConverter.formAddressDataToQuoteAddress(formData);
                        quote.billingAddress(quoteAddress);
                    }
                }                

                this._super(data, event);
            }
        });
    }
})