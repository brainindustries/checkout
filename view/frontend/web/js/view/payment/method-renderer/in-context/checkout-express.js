define([
    'Magento_Paypal/js/view/payment/method-renderer/in-context/checkout-express'
], function (parent) {
    'use strict';

    return parent.extend({
        defaults: {
            template: 'Shirtplatform_Checkout/payment/paypal-express-in-context'
        }
    });
});