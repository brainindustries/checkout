define([
    'Magento_Paypal/js/view/payment/method-renderer/paypal-express'
], function (parent) {
    'use strict';

    return parent.extend({
        defaults: {
            template: 'Shirtplatform_Checkout/payment/paypal-express'
        }
    });
});