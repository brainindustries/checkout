define([
    'Magento_Checkout/js/model/payment/renderer-list'
], function (rendererList) {
    'use strict';

    return function (parent) {

        var isContextCheckout = window.checkoutConfig.payment.paypalExpress.isContextCheckout,
                paypalExpress = 'Shirtplatform_Checkout/js/view/payment/method-renderer' +
                (isContextCheckout ? '/in-context/checkout-express' : '/paypal-express');

        for (var i in rendererList()) {
            if (rendererList()[i].type == 'paypal_express') {
                rendererList()[i].component = paypalExpress;
            }
        }

        return parent.extend({});
    }
});