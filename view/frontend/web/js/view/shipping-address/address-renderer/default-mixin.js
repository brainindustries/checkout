define([], function () {
    'use strict';

    return function (addressRenderer) {
        return addressRenderer.extend({
            defaults: {
                template: 'Shirtplatform_Checkout/shipping-address/address-renderer/default'
            },
            
            selectAddress: function() {                
                var address = this.address();
                var isPackstation = false;
                
                if (address.street.length && /packstation/i.test(address.street[0])) {
                    isPackstation = true;
                }
                else if (address.extension_attributes != undefined && address.extension_attributes.packstation_number) {
                    isPackstation = true;
                } 
                
                //sets isPackstation observable of the grandparent (shipping component)
                this.containers[0].containers[0].isPackstation(isPackstation);       
                this._super();
            }
        });
    };
});
