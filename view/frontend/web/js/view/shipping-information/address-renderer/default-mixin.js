define([
    'Magento_Checkout/js/model/quote',
    'Shirtplatform_Checkout/js/checkout-data'
], function (quote, shirtplatformCheckoutData) {
    'use strict';

    return function (defaultRenderer) {
        return defaultRenderer.extend({
            defaults: {
                template: 'Shirtplatform_Checkout/shipping-information/address-renderer/default'
            },           
            
            isZasielkovnaAddress: function() {
                var method = quote.shippingMethod();
                var methodName = method != null ? method.carrier_code + '_' + method.method_code : null;
                return methodName == window.checkoutConfig.zasielkovna_method_name || methodName == window.checkoutConfig.packetery_method_name;
            },                
            
            getZasielkovnaAddress: function() {
                return shirtplatformCheckoutData.getZasielkovnaAddress();
            }
        });
    };
});
