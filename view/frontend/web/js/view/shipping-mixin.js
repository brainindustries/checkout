define([
    'jquery',
    'ko',
    'uiRegistry',
    'mage/translate',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/action/set-shipping-information',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/address-converter',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/step-navigator',
    'Shirtplatform_Checkout/js/checkout-data'
], function (
    $,
    ko,
    uiRegistry,
    $t,
    selectShippingAddress,
    setShippingInformationAction,
    checkoutData,
    addressConverter,
    checkoutDataResolver,
    quote,
    stepNavigator,
    shirtplatformCheckoutData
) {
    'use strict';

    return function (shipping) {
        var logoData = [];
        var descriptionData = [];

        return shipping.extend({
            isPackstation: ko.observable(false),
            zasielkovnaApiKey: window.checkoutConfig.zasielkovna_key,
            deliveryBranchId: ko.observable(shirtplatformCheckoutData.getDeliveryBranchId()),
            deliveryBranchName: ko.observable(shirtplatformCheckoutData.getDeliveryBranchName()),
            country: window.checkoutConfig.defaultCountryId.toLocaleLowerCase(),

            defaults: {
                shippingFormTemplate: 'Shirtplatform_Checkout/shipping-address/form',
                shippingMethodListTemplate: 'Shirtplatform_Checkout/shipping-address/shipping-method-list',
                shippingMethodItemTemplate: 'Shirtplatform_Checkout/shipping-address/shipping-method-item',
            },

            initialize: function () {
                var self = this;
                this._super();
                let shippingAddress = quote.shippingAddress();

                //populate shipping method description and logo data
                if (typeof window.checkoutConfig.shippingMethod_description !== 'undefined') {
                    descriptionData = JSON.parse(window.checkoutConfig.shippingMethod_description);
                }
                if (typeof window.checkoutConfig.shippingMethod_logo !== 'undefined') {
                    logoData = JSON.parse(window.checkoutConfig.shippingMethod_logo);
                }

                if (this.usePackstation()) {
                    this.isPackstation(shirtplatformCheckoutData.getIsPackstation());

                    uiRegistry.async('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.street')(function (street) {
                        self.resolveAddressFields();
                    });

                    //click on packstation checkbox - show/hide the fields, set validation rules
                    this.isPackstation.subscribe(function (isPackstation) {
                        shirtplatformCheckoutData.setIsPackstation(isPackstation);
                        self.resolveAddressFields();

                        if (!self.isCustomerLoggedIn()) {
                            self.refreshShippingMethods();
                        }
                    });

                    //logged in customer - in case that the chosen shipping address is Packstation
                    if (!this.isPackstation() && this.isCustomerLoggedIn() && !quote.isVirtual()) {
                        // var shippingAddress = quote.shippingAddress();

                        if (shippingAddress && shippingAddress.street && shippingAddress.street[0] == 'Packstation') {
                            this.isPackstation(true);
                        }
                    }
                }
                //watch for change in vat_id value
                if (window.checkoutConfig.isBusinessNumberRequiredForVatId) {
                    uiRegistry.async('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.vat_id')(
                        function (vatId) {
                            self.setBusinessNumberValidationRule(vatId.value());
                            vatId.value.subscribe(function (newValue) {
                                self.setBusinessNumberValidationRule(newValue);
                            });
                        });
                }

                //initialize values for extension attributes inputs                
                if (shippingAddress.extension_attributes != undefined) {
                    for (let fieldName of Object.keys(shippingAddress.extension_attributes)) {
                        if (shippingAddress.extension_attributes[fieldName]) {
                            let field = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.' + fieldName);

                            if (field) {
                                field.value(shippingAddress.extension_attributes[fieldName]);
                            }
                        }
                    }
                }

                if (this.zasielkovnaApiKey) {
                    let head = document.getElementsByTagName('head')[0];
                    let el = document.createElement('script');
                    el.async = true;
                    el.src = 'https://widget.packeta.com/v6/www/js/library.js';
                    head.insertBefore(el, head.firstChild);
                }
            },

            initObservable: function () {
                var self = this;
                this._super();

                this.isZasielkovnaVisible = ko.computed(function () {
                    return self.getShippingMethodName(quote.shippingMethod()) == window.checkoutConfig.zasielkovna_method_name;
                });

                /**
                 * Case when the customer proceeds to step 2 with Zasielkovna selected, refreshes the page, and returns back to step 1. 
                 * When they change the shipping method, the incorrect (Zasielkovna) shipping address is selected
                 */
                quote.shippingMethod.subscribe(function (method) {                    
                    if (self.zasielkovnaApiKey && self.getShippingMethodName(method) != window.checkoutConfig.zasielkovna_method_name) {
                        checkoutDataResolver.applySelectedShippingAddress();
                    }                    
                });

                quote.shippingAddress.subscribe(function (newAddress) {
                    self.country = newAddress.countryId.toLocaleLowerCase();
                });

                return this;
            },

            getShippingMethodName: function (method) {
                return method != null ? method.carrier_code + '_' + method.method_code : null;
            },

            /**
             * Get shipping method description text
             * 
             * @param {String} carrier (deprecated)
             * @param {String} code
             * @returns {String}
             */
            getShippingMethodDescriptionText: function (carrier, code) {
                if (typeof descriptionData[code] === 'undefined') {
                    return '';
                }
                return descriptionData[code].description;
            },

            /**
             * Get shipping method logo
             *              
             * @param {String} carrier
             * @param {String} code
             * @returns {String} logo URL
             */
            getShippingMethodLogo: function (carrier, code) {
                if (carrier !== 'shirtplatformShipping' || typeof logoData[code] === 'undefined') {
                    return '';
                }

                return logoData[code];
            },

            /**
             * Show / hide packstation fields and street/street_number based on the checkbox
             * Sets the correct validation rules             
             */
            resolveAddressFields: function () {
                let postNumber = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.post_number');
                let packstationNumber = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.packstation_number');
                let streetNumber = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.street_number');
                let streetGroup = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.street');

                if (this.isPackstation()) {
                    postNumber.visible(true);
                    postNumber.validation['required-entry'] = true;
                    packstationNumber.visible(true);
                    packstationNumber.validation['required-entry'] = true;
                    streetGroup.visible(false);

                    for (let line of streetGroup.elems()) {
                        line.validation['required-entry'] = false;
                        line.value('');
                    }

                    if (streetNumber) {
                        streetNumber.validation['required-entry'] = false;
                        streetNumber.visible(false);
                    }
                } else {
                    postNumber.visible(false);
                    postNumber.validation['required-entry'] = false;
                    packstationNumber.visible(false);
                    packstationNumber.validation['required-entry'] = false;
                    streetGroup.visible(true);

                    //during the first load of the checkout page the elems were not initialized yet, hence this condition
                    if (streetGroup.elems()[0]) {
                        streetGroup.elems()[0]['required-entry'] = true;
                    }

                    if (streetNumber) {
                        streetNumber.validation['required-entry'] = true;
                        streetNumber.visible(true);
                    }
                }
            },

            refreshShippingMethods: function () {
                var address;

                if (this.isFormPopUpVisible()) {
                    var addressFlat = uiRegistry.get('checkoutProvider').shippingAddress;
                    address = addressConverter.formAddressDataToQuoteAddress(addressFlat);
                } else {
                    address = quote.shippingAddress();
                }
                selectShippingAddress(address);
            },

            /**
             * Set validation rule for business_number based on vat_id value
             * 
             * @param {String} vatIdValue             
             */
            setBusinessNumberValidationRule: function (vatIdValue) {
                var businessNumber = uiRegistry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.business_number');

                if (businessNumber && vatIdValue.trim() != '') {
                    businessNumber.validation['required-entry'] = true;
                    businessNumber.required(true);
                } else {
                    businessNumber.validation['required-entry'] = false;
                    businessNumber.required(false);
                    businessNumber.validate();
                }
            },

            /**
             * Pre-fill billing address form with shipping address data             
             */
            setShippingInformation: function () {
                if (!this.validateShippingInformation()) {
                    return;
                }

                quote.billingAddress(null);
                checkoutDataResolver.resolveBillingAddress();

                setShippingInformationAction().done(function (response) {
                    let foundMethod = false;
                    let methods = response.payment_methods;

                    for (let method in methods) {
                        if (methods[method].method == checkoutData.getSelectedPaymentMethod()) {
                            foundMethod = true;
                            break;
                        }
                    }

                    if (!foundMethod) {
                        checkoutData.setSelectedPaymentMethod(null);
                    }

                    stepNavigator.next();
                });

                if (checkoutData.getBillingAddressFromData() == null) {
                    checkoutData.setBillingAddressFromData(checkoutData.getShippingAddressFromData());

                    if (!window.checkoutConfig.displayBillingOnPaymentMethod && checkoutData.getBillingAddressFromData()) {
                        var billingAddressFields = uiRegistry.filter('customScope = billingAddressshared');
                        var shippingAddressFields = uiRegistry.filter('customScope = shippingAddress');

                        if (billingAddressFields && shippingAddressFields) {
                            for (var i in billingAddressFields) {
                                for (var j in shippingAddressFields) {
                                    if (billingAddressFields[i].inputName == shippingAddressFields[j].inputName) {
                                        billingAddressFields[i].value(shippingAddressFields[j].value());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                if (!this.isCustomerLoggedIn()) {
                    let shippingAddressData = addressConverter.quoteAddressToFormAddressData(quote.shippingAddress());
                    checkoutData.setShippingAddressFromData(shippingAddressData);
                }
            },

            /**
             * Is packstation enabled in admin?
             * 
             * @returns {Boolean}
             */
            usePackstation: function () {
                return window.checkoutConfig.usePackstation == 1;
            },

            /**
             * Check zasielkovna branch
             * 
             * @returns {Boolean}
             */
            validateShippingInformation: function () {
                var result = this._super();

                if (!result && this.isZasielkovnaSelected()) {
                    let billingAddressFields = uiRegistry.filter('customScope = billingAddress');
                    let shippingAddressFields = uiRegistry.filter('customScope = shippingAddress');

                    if (billingAddressFields && shippingAddressFields) {
                        for (let i in shippingAddressFields) {
                            for (let j in billingAddressFields) {
                                if (shippingAddressFields[i].inputName == billingAddressFields[j].inputName) {
                                    shippingAddressFields[i].value(billingAddressFields[j].value());
                                    break;
                                }
                            }
                        }
                    }

                    result = this._super();
                }

                if (result) {
                    var shippingMethodName = this.getShippingMethodName(quote.shippingMethod());

                    if (shippingMethodName == window.checkoutConfig.zasielkovna_method_name && !shirtplatformCheckoutData.getDeliveryBranchId()) {
                        this.errorValidationMessage($t('Please choose a delivery branch'));
                        result = false;
                    }
                }

                return result;
            },

            isZasielkovnaSelected: function () {
                return checkoutData.getSelectedShippingRate() == window.checkoutConfig.zasielkovna_method_name
                    || checkoutData.getSelectedShippingRate() == window.checkoutConfig.packetery_method_name;
            },

            /**
             * This function will receive either a pickup point object, or null if the user
             * did not select anything, e.g. if they used the close icon in top-right corner
             * of the widget, or if they pressed the escape key.
             * 
             * @param {Object} point         
             */
            showSelectedBranch: function (point) {
                if (point) {
                    let zasielkovnaAddress = $.extend(true, {}, quote.shippingAddress());
                    let streetParts = point.street.split(' ');
                    let streetNum = streetParts[streetParts.length - 1];
                    let street = point.street.replace(' ' + streetNum, '');

                    shirtplatformCheckoutData.setDeliveryBranchName(point.place);
                    shirtplatformCheckoutData.setDeliveryBranchId(point.id);

                    zasielkovnaAddress.street = [street];
                    zasielkovnaAddress.city = point.city;
                    zasielkovnaAddress.postcode = point.zip;
                    zasielkovnaAddress.countryId = point.country.toUpperCase();
                    zasielkovnaAddress.street_number = streetNum;
                    zasielkovnaAddress.care_of = point.place;
                    zasielkovnaAddress.customerAddressId = null;

                    if (zasielkovnaAddress.extension_attributes === undefined) {
                        zasielkovnaAddress.extension_attributes = {};
                    }

                    zasielkovnaAddress.extension_attributes['street_number'] = streetNum;
                    zasielkovnaAddress.extension_attributes['care_of'] = point.place;
                    shirtplatformCheckoutData.setZasielkovnaAddress(zasielkovnaAddress);
                }
            },

            /**
             * Get Zasielkovna delivery_branch_id
             * 
             * @returns {int}
             */
            getDeliveryBranchId: function () {
                return shirtplatformCheckoutData.getDeliveryBranchId();
            },

            /**
             * Get Zasielkovna delivery branch name
             * 
             * @returns {string}
             */
            getDeliveryBranchName: function () {
                return shirtplatformCheckoutData.getDeliveryBranchName();
            }
        });
    }
});